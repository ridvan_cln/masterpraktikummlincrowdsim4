import numpy as np

def transform(X, n_components):
    """Calculating PCA on the matrix X 

    Args:
        X: Given matrix.
        n_components: number of components to use.

    Returns:
        reconstructed: Matrix reconstructed with the given amount of components n_components (Original shape of X)
        transformed: Matrix transformed with a dimensionality reduction according to n_components
    """
    U, s, VT = np.linalg.svd(X)
    Sigma = np.zeros((X.shape[0], X.shape[1]))
    if X.shape[1] <= X.shape[0]:
        Sigma[:X.shape[1], :X.shape[1]] = np.diag(s)
    else:
        Sigma[:X.shape[0], :X.shape[0]] = np.diag(s)
    # Only keep needed components
    Sigma = Sigma[:, :n_components]
    VT = VT[:n_components, :]
    # Original shape
    reconstructed = U.dot(Sigma.dot(VT))
    # Reduced dimensionality shape
    transformed = U.dot(Sigma)
    return reconstructed, transformed

def explained_variance(transformed, n_components, n_samples):
    """Calculates the explained variance

    Args:
        transformed: Given matrix.
        n_components: number of components to use.
        n_samples: number of data samples

    Returns:
        explained_variance: An array containing the explained variance of each component
    """
    return np.array([(transformed[:, i]**2).sum()/(n_samples-1) for i in range(n_components)])

def explained_variance_ratio(X, n_components):
    """Calculates the explained variance ratio

    Args:
        X: Given matrix.
        n_components: number of components to use.

    Returns:
        explained_variance_ratio: An array containing the explained variance ratio of each component
    """
    _, s, _ = np.linalg.svd(X)
    return np.array([s[i]**2 / sum(s**2) for i in range(n_components)])

if __name__ == "__main__":
    pass