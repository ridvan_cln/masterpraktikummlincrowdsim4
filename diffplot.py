import matplotlib.pyplot as plt
import numpy as np

import plotly
import plotly.graph_objs as go

class diffplot:
    def __init__(self, row=1, col=2):
        self.row = row
        self.col = col
        self.pic = 1

    def plot_scatter(self,fig,x,y,color='blue',title='Scatter plot', x_axis='x', y_axis='y'):
        """Plotter.

        Args:
            fig (figure): The figure.
            x (vec): First axis.
            y (vec): Second axis.
            color (str, optional): Color for the samples. Defaults to 'blue'.
            title (str, optional): Title for the plot. Defaults to 'Scatter plot'.
            x_axis (str, optional): Name of the x-axis. Defaults to 'x'.
            y_axis (str, optional): Name of the y-axis. Defaults to 'y'.
        """        
        ax = fig.add_subplot(self.row,self.col,self.pic)
        cmap = plt.cm.get_cmap("jet")

        ax.scatter(x,y,c=color,cmap=cmap)
        ax.set_title(title)
        ax.set_xlabel(x_axis)
        ax.set_ylabel(y_axis)
        
        self.pic += 1
        
    def plot_scatter_3D(self,X,color='blue'):
        """Plotter for 3D plots using plotly.

        Args:
            X (Matrix): Containg all axis which are needed to plot
            color (str, optional): Color for the samples. Defaults to 'blue'.
        """        
        plotly.offline.init_notebook_mode()
        # Add 3d scatter plot
        #ax = fig.add_subplot(self.row,self.col,self.pic, projection='3d')
        #ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=color, cmap=plt.cm.Spectral)
        #ax.view_init(4, -72)

        # Configure Plotly to be rendered inline in the notebook.
        plotly.offline.init_notebook_mode()

        # Configure the trace.
        trace = go.Scatter3d(
            x=X[:, 0],  # <-- Put your data instead
            y=X[:, 1],  # <-- Put your data instead
            z=X[:, 2],  # <-- Put your data instead
            mode='markers',
            marker={
                'size': 5,
                'opacity': 0.8,
                'color': color,
                'colorscale': 'jet'
            }
        )

        # Configure the layout.
        layout = go.Layout(
            margin={'l': 0, 'r': 0, 'b': 0, 't': 0}
        )

        data = [trace]

        plot_figure = go.Figure(data=data, layout=layout)

        # Render the plot.
        plotly.offline.iplot(plot_figure)
 
