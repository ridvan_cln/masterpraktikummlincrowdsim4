import time
import numpy as np
import tensorflow as tf
from tensorflow.contrib.slim import fully_connected as fc
import matplotlib.pyplot as plt 
#%matplotlib inline
#print(tf.__version__)

class VariationalAutoencoder(object):

    def __init__(self, learning_rate=1e-3, batch_size=128, n_z=2, input_dim=784, n_neurons_enc=256, n_neurons_dec=256):
        """Set the hyperparameters, the default values correspond to the ones asked for in Task3

        Args:
            learning_rate (float, optional): determine the learning rate for the model. Defaults to 1e-3.
            batch_size (int, optional): give the batch size. Defaults to 128.
            n_z (int, optional): The latent dimension. Defaults to 2.
            input_dim (int, optional): The input dimension (data dimension). Defaults to 784.
            n_neurons_enc (int, optional): number of neurons in each layer of the encoder (units). Defaults to 256.
            n_neurons_dec (int, optional): number of neurone in each layer of decoder (units). Defaults to 256.
            sess : used to initialize running a session for training and testing
        """        
        # Set hyperparameters
        self.learning_rate = learning_rate
        self.batch_size = batch_size
        self.n_z = n_z
        self.input_dim = input_dim
        self.n_neurons_enc = n_neurons_enc
        self.n_neurons_dec = n_neurons_dec
        
        # Build the graph
        self.build()

        # Initialize paramters
        self.sess = tf.InteractiveSession()
        self.sess.run(tf.global_variables_initializer())

    def build(self):
        """Build the network and the loss function

            Args:
            x: the dataset
            f1, f2: fully connected layers in encoder with ReLU activation function and 256 units
            z_mu: mean for posterior distribution
            z_log_sigma_sq: variance for posterior distibution
            eps: epsilon used to reparametrize z, it follows a random normal distribution

            g1,g2: fully connected layerz in decoder with ReLU distribution and 256 units
            x_hat: decoder's output
            latent_loss: latent loss (from KL divergence)
            recon_loss: reconstruction loss
            total_loss: -elbo, calculated from the two previous losses
            train_op: Adam optimizer minimized by -elbo
        """        
        tf.reset_default_graph()
        
        self.x = tf.placeholder(
            name='x', dtype=tf.float32, shape=[None, self.input_dim])
        # Encode
        # x -> z_mean, z_sigma -> z
        f1 = fc(self.x, self.n_neurons_enc, scope='enc_fc1', activation_fn=tf.nn.relu)
        f2 = fc(f1, self.n_neurons_enc, scope='enc_fc2', activation_fn=tf.nn.relu)
        self.z_mu = fc(f2, self.n_z, scope='enc_fc3_mu', 
                       activation_fn=None)
        self.z_log_sigma_sq = fc(f2, self.n_z, scope='enc_fc3_sigma', 
                                 activation_fn=None)
        eps = tf.random_normal(
            shape=tf.shape(self.z_log_sigma_sq),
            mean=0, stddev=1, dtype=tf.float32)
        self.z = self.z_mu + tf.sqrt(tf.exp(self.z_log_sigma_sq)) * eps

        # Decode
        # z -> x_hat
        g1 = fc(self.z, self.n_neurons_dec, scope='dec_fc1', activation_fn=tf.nn.relu)
        g2 = fc(g1, self.n_neurons_dec, scope='dec_fc2', activation_fn=tf.nn.relu)
        self.x_hat = fc(g2, self.input_dim, scope='dec_fc3', 
                        activation_fn=tf.sigmoid)

        # Loss
        # Reconstruction loss: minimize the cross-entropy loss
        # H(x, x_hat) = -\Sigma x*log(x_hat) + (1-x)*log(1-x_hat)
        epsilon = 1e-10
        recon_loss = -tf.reduce_sum(
            self.x * tf.log(epsilon+self.x_hat) + 
            (1-self.x) * tf.log(epsilon+1-self.x_hat), 
            axis=1
        )
        self.recon_loss = tf.reduce_mean(recon_loss)

        # Latent loss
        # KL divergence: measure the difference between two distributions
        # Here we measure the divergence between the latent distribution and N(0, 1)
        latent_loss = -0.5 * tf.reduce_sum(
            1 + self.z_log_sigma_sq - tf.square(self.z_mu) - 
            tf.exp(self.z_log_sigma_sq), axis=1)
        self.latent_loss = tf.reduce_mean(latent_loss)

        self.total_loss = self.recon_loss + self.latent_loss
        self.train_op = tf.train.AdamOptimizer(
            learning_rate=self.learning_rate).minimize(self.total_loss)
        
        self.losses = {
            'recon_loss': self.recon_loss,
            'latent_loss': self.latent_loss,
            'total_loss': self.total_loss,
        }        
        return

    def run_single_step(self, x):
        """Execute the forward and the backward pass

        Args:
            x : dataset

        Returns:
            losses: the three losses
        """        
        _, losses = self.sess.run(
            [self.train_op, self.losses],
            feed_dict={self.x: x}
        )
        return losses

    def reconstructor(self, x):
        """Reconstructing images

        Args:
            x : data 

        Returns:
            X_hat: decoder's output, the reconstructed image
        """        
        x_hat = self.sess.run(self.x_hat, feed_dict={self.x: x})
        return x_hat

    # z -> x
    def generator(self, z):
        """Generating new data

        Args:
            z : latent variable, encoded representation

        Returns:
            x_hat: new generated image, decoder's output
        """        
        x_hat = self.sess.run(self.x_hat, feed_dict={self.z: z})
        return x_hat
    
    # x -> z
    def transformer(self, x):
        """Latent representation

        Args:
            x : data

        Returns:
            z : latent variable
        """        
        z = self.sess.run(self.z, feed_dict={self.x: x})
        return z

def trainer(model_class, data, learning_rate=1e-3, 
            batch_size=128, num_epoch=1, n_z=2, input_dim=784, log_step=5, num_sample=55000, n_neurons_enc=256, n_neurons_dec=256 ):
    """Method to train the model

    Args:
        model_class (class): the used model, VariationalAutoencoder here
        data (array, dimension (num_sample, input_dim)): the dataset used for training
        learning_rate (float): the learning rate. Defaults to 1e-3.
        batch_size (int): the batch size. Defaults to 128.
        num_epoch (int): the number of epochs. Defaults to 1.
        n_z (int): the latent dimension. Defaults to 2.
        input_dim (int): the input dimension. Defaults to 784.
        log_step (int): the log steps number. Defaults to 5.
        num_sample (int): the number of samples in the training set. Defaults to 55000.
        n_neurons_enc (int): number of neurons in each layer in encoder. Defaults to 256.
        n_neurons_dec (int): number of enurons in each layer in decoder. Defaults to 256.

    Returns:
        model: used in data reconstruction and generation functions
        array: array with the total loss values per epoch, to generate a loss vs epochs plot
    """    
    # Create a model    
    model = model_class(
        learning_rate=learning_rate, batch_size=batch_size, n_z=n_z, input_dim=input_dim, n_neurons_enc=n_neurons_enc, n_neurons_dec=n_neurons_dec)
    array = np.zeros((2,num_epoch))

    # Training loop    
    for epoch in range(num_epoch):
        start_time = time.time()
        array[0,epoch] = epoch
        # Run an epoch
        for iter in range(num_sample // batch_size):
            # Get a batch
            batch = data.train.next_batch(batch_size)
            # Execute the forward and backward pass 
            # Report computed loss
            losses = model.run_single_step(batch[0])
        end_time = time.time()
        #print(losses)
        array[1,epoch] = losses['total_loss']
        # Log the loss
        if epoch % log_step == 0 or epoch == 1:
            log_str = '[Epoch {}] '.format(epoch)
            for k, v in losses.items():
                log_str += '{}: {:.3f}  '.format(k, v)
            log_str += '({:.3f} sec/epoch)'.format(end_time - start_time)
            print(log_str)       
           
    print('Done!')
    return model, array

def test_reconstruction(model, data, h=28, w=28, batch_size=15):
    """Reconstruct images

    Args:
        model : the trained model
        data : the data to be used
        h (int): height of image.  Defaults to 28.
        w (int): width of image. Defaults to 28.
        batch_size (int): Batch size. Defaults to 15.
    
    Returns:
        reconstructed images and the original ones right next to it on the right
    """    
    # Test the trained model: reconstruction
    batch = data.test.next_batch(batch_size)
    x_reconstructed = model.reconstructor(batch[0])
    #print(len(x_reconstructed))
    #n = np.sqrt(batch_size).astype(np.int32)
    I_reconstructed = np.empty((h*3, 2*w*5))
    acc=[]
    for i in range(3):
        for j in range(5):
            abs_diff=(abs(x_reconstructed[i*3+j, :] - batch[0][i*5+j, :])) 
                                #(batch[0][i*5+j, :] - x_reconstructed[i*3+j, :])]),key=abs)
            acc.append(np.sum(abs_diff))
            x = np.concatenate(
                (x_reconstructed[i*3+j, :].reshape(h, w), 
                 batch[0][i*5+j, :].reshape(h, w)),
                axis=1
            )
            #print(accuracy)
            #accuracy=np.array(accuracy)
            #print(acc)
            #print(x.shape, type(x_reconstructed), x_reconstructed[i*3+j, :].shape, batch[0][i*5+j, :].shape)
            I_reconstructed[i*h:(i+1)*h, j*2*w:(j+1)*2*w] = x
    print("Accuracy for each image: ", acc)
    plt.figure(figsize=(10, 20))
    plt.imshow(I_reconstructed, cmap='gray')
    plt.show()

def test_generation(model, z=None, h=28, w=28, batch_size=15):
    """Generates new images

    Args:
        model : the trained model
        z : latent variable. Defaults to None.
        h (int): height of image. Defaults to 28.
        w (int): width of image. Defaults to 28.
        batch_size (int): batch size. Defaults to 15.
    
    Returns:
        Generated new images
    """    
    # Test the trained model: generation
    # Sample noise vectors from N(0, 1)
    if z is None:
        z = np.random.normal(size=[batch_size, model.n_z])
    x_generated = model.generator(z)    

    #n = np.sqrt(batch_size).astype(np.int32)
    I_generated = np.empty((h*3, w*5))
    for i in range(3):
        for j in range(5):
            I_generated[i*h:(i+1)*h, j*w:(j+1)*w] = x_generated[i*3+j, :].reshape(h, w)
            
    plt.figure(figsize=(8, 8))
    plt.imshow(I_generated, cmap='gray')
    plt.title('generated_data at epoch')
    plt.show

def test_transformation(model_2d, data, batch_size=10000):
    """Test the trained model: transformation

    Args:
        model_2d : the model with 2d latent dim
        data : dataset used
        batch_size (int): batch size. Defaults to 10000.
    """    
    assert model_2d.n_z == 2
    batch = data.test.next_batch(batch_size)
    z = model_2d.transformer(batch[0])
    plt.figure(figsize=(10, 8)) 
    plt.scatter(z[:, 0], z[:, 1], c=np.argmax(batch[1], 1), s=20)
    plt.colorbar()
    plt.grid()

