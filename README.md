# MasterPraktikumMLinCrowdSim4

Python 3.7 is needed and Poetry:

https://python-poetry.org/docs/
```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```

## Install the dependencies
```bash
poetry install
```

Dependencies are in pyproject.toml. Installing tensorflow the following commands should be used.
First run:
```bash
poetry run pip install -U pip
```
then:
```bash
poetry run pip install tensorflow==1.14 
```
Using jupyter notebook is recommended.