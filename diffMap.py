import numpy as np

from scipy.spatial import distance_matrix
from scipy.linalg import eigh
import matplotlib.pyplot as plt
from scipy.linalg import expm
from numpy.linalg import matrix_power

from sklearn import decomposition
from sklearn.preprocessing import StandardScaler

def distance(x,y,p):
    """Calculating the distance between 
    two vectors x,y in a corrsponding norm p. 

    Args:
        x (vec): First vector.
        y (vec): Second vector.
        p (int): Norm.

    Returns:
        np.matrix: Distance matrix
    """    
    return distance_matrix(x,y,p)

def diffmap(X,L,epsi=0.05):
    """Calculating the eigenfunctions based on the algorithm given in the lecture

    Args:
        X (Dictionary): The dataset.
        L (Integer): Number of eigenfunction which should be calculated.
        epsi (float, optional): Kernel band: Epsilon. Defaults to 0.05.

    Returns:
        vector, matrix, vector: eigenvalues in a vector, eigenfunctions in a matrix, scaled eigenfucntions in a matrix 
    """    
    # Compute the kernel Matrix
    D = distance(X,X,2)
    epsilon = epsi*(D.max())
    #W = expm(-np.matrix_power(D,2)/epsilon)
    W = np.exp(-np.power(D,2)/epsilon) #NxN matrix
    #Normalization, keeps being symmetric
    P = np.diag(np.sum(W,axis=1))
    K = np.linalg.inv(P)@W@np.linalg.inv(P)
    Q = np.diag(np.sum(K,axis=1))
    T = np.sqrt(np.linalg.inv(Q))@K@np.sqrt(np.linalg.inv(Q))
    
    # Compute eigenfunctions, extract L largest.
    L = L+1

    v_val,w_vec = eigh(T,eigvals=(T.shape[0]-L,T.shape[0]-1))
    
    lam = np.sqrt(np.power(v_val,1/epsilon))
    phi = np.sqrt(np.linalg.inv(Q))@w_vec

    lam = np.flip(lam,0)
    phi = np.flip(phi,1)

    Phi = np.multiply(phi,lam)

    return lam,phi,Phi

def pca(X, components=2):
    """Wrapper for the sklearn PCA function.

    Args:
        X (Dictionary): The Dataset.
        components (int, optional): Number of principal components which should be calculated. Defaults to 2.

    Returns:
        matrix: principal components.
    """    
    pca = decomposition.PCA(n_components=components)
    
    #demean and scale
    x_std = StandardScaler().fit_transform(X)
    return pca.fit_transform(x_std)

if __name__ == "__main__":
    pass
